# wxpay

#### 介绍
智慧科技微信支付工具包

#### 软件架构
软件架构说明


#### 安装教程
实例化微信工具对象

APPID 微信公众号 appId
APPSECRET 微信公众号的 appsecret
MCHID 微信支付商户id
MCHCERT 微信支付商户API证书文件路径
MCHKEY 微信支付商户API证书私钥文件路径
SERIAL_NO 微信支付商户API证书编号
APIV3_KEY 微信支付商户密钥
WX_CERT_PATH 微信平台证书保存路径（需要具有读写权限）

$wt = new WxTools(APPID,APPSECRET,MCHID,MCHCERT,MCHKEY,SERIAL_NO,APIV3_KEY,WX_CERT_PATH)

#### 使用说明

#### 参与贡献
