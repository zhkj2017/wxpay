<?php

namespace zhihuikeji\Wxpay;

// 微信JSAPI下单接口
const JSAPI_URL = 'https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi';

class WxTools
{
    const HEADER = [
        'Content-Type: application/json',
        'Accept: application/json',
        'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
    ];
    static private $access_token;
    private static $access_token_url;
    private static $appid;
    private static $appsecret;
    private static $mchid;
    private static $mchcert;
    private static $mchkey;
    private static $serial_no;
    private static $wx_cert_path;
    private static $apiv3_key;

    const WX_CERT_PREFIX = 'WxCert_';

    function __construct($appid, $appsecret, $mchid, $mchcert, $mchkey, $serial_no, $apiv3_key, $wx_cert_path)
    {
        self::$appid = $appid;
        self::$appsecret = $appsecret;
        self::$mchid = $mchid;
        self::$mchcert = $mchcert;
        self::$mchkey = $mchkey;
        self::$serial_no = $serial_no;
        self::$apiv3_key = $apiv3_key;
        self::$wx_cert_path = rtrim($wx_cert_path, '/') . DIRECTORY_SEPARATOR;
    }

    function set_access_token_url($url)
    {
        self::$access_token_url = $url;
        return true;
    }


    /**
     * 获取curl对象
     * @param $url 请求地址
     * @param $method 请求方法
     * @return false|resource
     */

    private function get_curl($url, $method = 'GET')
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_HTTPHEADER, self::HEADER);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        if (strtolower($method) == 'POST') {
            curl_setopt($curl, CURLOPT_POST, 1);
        }
        return $curl;
    }

    /** 从统一的接口获取access_token
     * @return mixed
     * @throws \Exception
     */
    private static function get_access_token()
    {
        if (empty(self::$access_token) || self::$access_token['expire'] < time() + 300) {
            $curl = curl_init(self::$access_token_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
            $res = curl_exec($curl);
            $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($status_code != 200) {
                throw new \Exception('微信公众号Access_toke获取失败。');
            }
            curl_close($curl);
            $res = json_decode($res, true);
            self::$access_token = $res['access_token'];
        }
        return self::$access_token;
    }

    /**
     * 生成随机字符串
     */
    function get_random_str()
    {
        return md5(random_bytes(32));
    }

    private function get_mchkey()
    {
        return openssl_get_privatekey(file_get_contents(self::$mchkey));
    }

    private function sha256_sign($data)
    {
        $signature = '';
        $key = $this->get_mchkey();
        openssl_sign($data, $signature, $key, 'sha256WithRSAEncryption');
        openssl_pkey_free($key);
        $signature = base64_encode($signature);
        return $signature;
    }

    private function set_http_authorization(&$ch, $method, $body = '')
    {
        $time = time();
        $random_str = $this->get_random_str();
        $parased_url = parse_url(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL));
        $path = $parased_url['path'];
        $path .= empty($parased_url['query']) ? '' : '?' . $parased_url['query'];
        $data = $method . "\n" . $path . "\n" . $time . "\n" . $random_str . "\n" . $body . "\n";
        $signature = $this->sha256_sign($data);
        $authorization = 'Authorization:WECHATPAY2-SHA256-RSA2048 mchid="' . self::$mchid . '",nonce_str="' . $random_str . '",signature="' . $signature . '",timestamp="' . $time . '",serial_no="' . self::$serial_no . '"';
        $header = self::HEADER;
        $header[] = $authorization;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        return $ch;
    }

    /**
     * 获取下单前的预支付交易会话标识
     *
     * @param $out_trade_no '商户订单id'
     * @param $description '订单藐视'
     * @param $notify_url '异步通知地址'
     * @param $amount '交易金额，单位：分'
     * @param $openid '支付者的openid'
     * @param string $expire '有效期时间'
     */
    private function get_prepay_id($out_trade_no, $description, $notify_url, $amount, $openid, $expire = '')
    {
        $data = [
            'mchid' => self::$mchid,
            'out_trade_no' => $out_trade_no,
            'description' => $description,
            'appid' => self::$appid,
            'notify_url' => $notify_url,
            'amount' => [
                'total' => round($amount * 100, 0),
                'currency' => 'CNY',
            ],
            'payer' => [
                'openid' => $openid,
            ]
        ];
        if (!empty($expire)) {
            $data['time_expire'] = date(DATE_RFC3339, $expire);
        }
        $json = json_encode($data);
        $curl = $this->get_curl(JSAPI_URL, 'POST');
        $this->set_http_authorization($curl, 'POST', $json);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
        $res = curl_exec($curl);
        $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $res = json_decode($res, false);
        if ($status_code != 200) {
            throw new \Exception('订单编号：'.$out_trade_no.'，微信支付申请失败，原因：' . $res->message . '，代码Code：' . $res->code);
        }
        return $res->prepay_id;
    }

    /**
     * 从微信服务器上抓取openid信息
     *
     * @param $appid
     * @param $secret
     * @param $code
     * @return mixed
     * @throws
     */
    function fetch_openid($code)
    {
        $url = "https://api.weixin.qq.com/sns/oauth2/access_token";
        $query = array(
            'appid' => self::$appid,
            'secret' => self::$appsecret,
            'code' => $code,
            'grant_type' => 'authorization_code'
        );
        $url .= '?' . http_build_query($query);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_HTTPHEADER, self::HEADER);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $res = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($res);
        //取出openid
        if (!empty($data->errcode)) {
            throw new \Exception($data->errmsg, $data->errcode);
        }
        return $data->openid;
    }

    /**
     * 通过用户授权code调用微信jsapi支付的参数
     * @param $out_trade_no
     * @param $description
     * @param $notify_url
     * @param $amount
     * @param $auth_code
     * @param string $expire
     */
    function get_jsapi($out_trade_no, $description, $notify_url, $amount, $auth_code, $expire = '')
    {
        $openid = $this->fetch_openid($auth_code);
        $this->get_jsapi_by_openid($out_trade_no, $description, $notify_url, $amount, $openid, $expire);
    }

    /**
     * 通过用户openid调用微信jsapi支付的参数
     * @param $out_trade_no
     * @param $description
     * @param $notify_url
     * @param $amount
     * @param $auth_code
     * @param string $expire
     */
    function get_jsapi_by_openid($out_trade_no, $description, $notify_url, $amount, $openid, $expire = '')
    {
        $openid = $openid;
        $prepay_id = $this->get_prepay_id($out_trade_no, $description, $notify_url, $amount, $openid, $expire = '');
        $time = time();
        $nonceStr = $this->get_random_str();
        $package = 'prepay_id=' . $prepay_id;
        $data = self::$appid . "\n" . $time . "\n" . $nonceStr . "\n" . $package . "\n";
        $paySign = $this->sha256_sign($data);
        $res = [
            'appId' => self::$appid,
            'timeStamp' => strval($time),
            'nonceStr' => $nonceStr,
            'package' => $package,
            'signType' => 'RSA',
            'paySign' => $paySign,
        ];
        echo json_encode($res);
    }


    /**
     * 解析请求头信息
     * @return array
     */
    function em_getallheaders()
    {
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }

    /**
     * 验证v3签名的信息
     */
    function check_sign_v3()
    {
        $headers = $this->em_getallheaders();
        $signature = $headers['Wechatpay-Signature'];
        if (empty($signature)) {
            throw new \Exception('微信签名不能为空。');
        }
        $body = file_get_contents('php://input');
        $response_check_str = $headers['Wechatpay-Timestamp'] . "\n" . $headers['Wechatpay-Nonce'] . "\n" . $body . "\n";
        $signature = base64_decode($signature);
        $cert = $this->get_wx_cert($headers['Wechatpay-Serial']);
        // 取出证书中的公钥
        $public_key = openssl_pkey_get_public($cert);
        $verify = openssl_verify($response_check_str, $signature, $public_key, OPENSSL_ALGO_SHA256);
        return $verify === 1;
    }

    /**
     * 从微信服务器上获取平台证书
     */

    function fetch_wx_cert()
    {
        $url = 'https://api.mch.weixin.qq.com/v3/certificates';
        $ch = $this->get_curl($url);
        $this->set_http_authorization($ch, 'GET');
        $res = curl_exec($ch);
        curl_close($ch);
        //echo $res;
        $data = json_decode($res);
        //取出openid
        if (!empty($data->errcode)) {
            throw new \Exception($data->errmsg, $data->errcode);
        } else {
            if (is_dir(self::$wx_cert_path)) {
                $dh = opendir(self::$wx_cert_path);
                while ($file = readdir($dh) !== false) {
                    if (substr($file, 0, strlen(self::WX_CERT_PREFIX)) == self::WX_CERT_PREFIX) {
                        unlink($file);
                    }
                }
            } else {
                throw new \Exception('保存微信证书的文件夹不存在，请检查。');
            }
            $certs = $data->data;
            foreach ($certs as $item) {
                $cert = $this->decode_ciphertext($item->encrypt_certificate->ciphertext, $item->encrypt_certificate->nonce, $item->encrypt_certificate->associated_data);
                file_put_contents(self::$wx_cert_path . self::WX_CERT_PREFIX . $item->serial_no . '.pem', $cert);
                return $cert;
            }
        }
    }

    /**
     * 对微信服务器回传信息进行解密
     * @param $ciphertext '密文字符串'
     * @param $nonceStr '随机字符串'
     * @param string $associatedData '附加信息'
     * @return false|string
     * @throws \SodiumException
     */
    function decode_ciphertext($ciphertext, $nonceStr, $associatedData = '')
    {
        $ciphertext = \base64_decode($ciphertext);
        if (strlen($ciphertext) <= 16) {
            return false;
        }
        if (function_exists('\sodium_crypto_aead_aes256gcm_is_available') && \sodium_crypto_aead_aes256gcm_is_available()) {
            return \sodium_crypto_aead_aes256gcm_decrypt($ciphertext, $associatedData, $nonceStr, self::$apiv3_key);
        }
        throw new \Exception('AEAD_AES_256_GCM需要PHP 7.1以上或者安装libsodium-php');
    }

    function get_wx_cert($serial_no = '')
    {
        if (empty($serial_no)) {
            $dh = opendir(self::$wx_cert_path);
            while ($file = readdir($dh) !== false) {
                if (substr($file, 0, strlen(self::WX_CERT_PREFIX)) == self::WX_CERT_PREFIX) {
                    return file_get_contents($file);
                }
            }
            return $this->fetch_wx_cert();
        } else {
            $cert_file = self::$wx_cert_path . self::WX_CERT_PREFIX . $serial_no . '.pem';
            if (!file_exists($cert_file)) {
                $this->fetch_wx_cert();
            }
            if (file_exists($cert_file)) {
                return file_get_contents($cert_file);
            } else {
                throw new \Exception('微信证书不存在，请检查编号是否正确。');
            }
        }
    }

    /**
     * 退款申请
     *
     * @param $out_trade_no '商户订单号'
     * @param $out_refund_no '单次退款编号'
     * @param $refund_amount '单次退款金额'
     * @param $total_amount '订单总金额'
     * @param string $notify_url '退款结果异步通知地址'
     * @param string $reason '退款原因'
     * @return mixed
     * @throws \Exception
     */
    function refund($out_trade_no, $out_refund_no, $refund_amount, $total_amount, $notify_url = '', $reason = '')
    {
        $url = 'https://api.mch.weixin.qq.com/v3/refund/domestic/refunds';
        $ch = $this->get_curl($url, 'POST');
        $data = [
            // 商户订单号
            'out_trade_no' => $out_trade_no,
            // 单次退款编号
            'out_refund_no' => $out_refund_no,
            //
            'amount' => [
                // 单次退款金额
                'refund' => intval($refund_amount * 100),
                // 订单总金额
                'total' => intval($total_amount*100),
                // 币种
                'currency'=>'CNY',
            ],
        ];
        // 退款原因
        if (!empty($reason)){
            $data['reason']=$reason;
        }
        // 退款异步通知地址
        if(!empty($notify_url)){
            $data['notify_url']=$notify_url;
        }
        $body = json_encode($data);

        $this->set_http_authorization($ch, 'POST', $body);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$body);
        $res = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $res = json_decode($res);
        if ($status_code != 200) {
            throw new \Exception('微信退款申请失败，原因：' . $res->message . '。错误代码：' . $res->code);
        }
        return $res;
    }

    /**
     * 单笔退款查询
     * @param $out_refund_no '单笔退款编号'
     * @return mixed
     */
    function refund_query($out_refund_no){
        $url = 'https://api.mch.weixin.qq.com/v3/refund/domestic/refunds/'.$out_refund_no;
        $ch = $this->get_curl($url,'GET');
        $this->set_http_authorization($ch,'GET');
        $res = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $res=json_decode($res);
        return $res;
    }
}